#!/bin/bash

#set -e
export PATH="/usr/bin:/bin:/usr/local/bin"

#COLOR FORMATTING
YELLOW="\e[93m"
RED='\e[31m'
NC="\e[39m"

CI__SERVICE_ACCOUNT_NAME="tiller-deployer"
CI__NAMESPACE="ci"

SCRIPTS_PATH=./scripts

#######################
# DEV NAMESPACE SETUP #
#######################
echo -e "${RED}=== DEVELOPPEMENT'S CLUSTER SETUP ===\n${NC}"

# Create all kubernetes components for CD namespace in cluster B
export KUBECONFIG=$CI_KUBECONFIG
bash ${SCRIPTS_PATH}/create_k8s_components.sh ${CI__SERVICE_ACCOUNT_NAME} ${CI__NAMESPACE}

# Setup Gitlab Registry
bash ${SCRIPTS_PATH}/registry_setup.sh ${REGISTRY_TOKEN} ${CI__NAMESPACE}

# Install gilab-runner
cp values.yaml ci_custom_values.yaml
sed -i 's/GROUP_TOKEN/'"${CI__GROUP_TOKEN}"'/g' ci_custom_values.yaml
sed -i 's|JOB_TAGS_LIST||g' ci_custom_values.yaml

helm repo add gitlab https://charts.gitlab.io
helm --namespace ${CI__NAMESPACE}  del gitlab-runner || echo;
helm install gitlab-runner --namespace ${CI__NAMESPACE} \
	 -f ci_custom_values.yaml \
	 gitlab/gitlab-runner
rm -f ci_custom_values.yaml
