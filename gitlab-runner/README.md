# Gitlab Manual Integration

## Continuous integration

### How-to

The following steps represent how to integrate gitlab-runner on the namespace "ci".

0. Create a Kubernetes cluster
1. Fetch the kubeconfig
2. Set the variable BODZEN_GROUP_TOKEN with Bodzen's group token from the [Settings](https://gitlab.com/groups/bodzen/-/settings/ci_cd)
3. Generate a new read-only registry token from the [account's panel](https://gitlab.com/profile/personal_access_tokens)
4. Execute the script
```
bash setup_ci.sh
```

## Continous deployment

The following steps represent how to integrate gitlab-runner on the namespace "cd", to deploy on the cluster "do-fra1-krak".

0. Create a K8s cluster "do-fra1-bodzen-dev"
1. Create a K8s cluster "do-fra1-krak"
2. Fetch the kubeconfig
3. Set the variable BODZEN_GROUP_TOKEN with Bodzen's group token from the [Settings](https://gitlab.com/groups/bodzen/-/settings/ci_cd)
4. Generate a new read-only registry token from the [account's panel](https://gitlab.com/profile/personal_access_tokens)
5. Execute the script
6. Genereate a kubeconfig for krak
```
kubectl config use-context do-fra1-krak &&
bash scripts/generate_config.sh tiller-deployer omega &&
base64 k8s-tiller-deployer-omega-kubeconf | tr -d '\n' | xclip -selection c &&
rm k8s-tiller-deployer-omega-kubeconf
```
7. Paste the clipboard content in the variable "KRAK_KUBECONF" in [bodzen-helm CI_CD settings](https://gitlab.com/groups/bodzen/bodzen-helm/-/settings/ci_cd).
