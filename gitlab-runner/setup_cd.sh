#!/bin/bash

set -e
export PATH="/usr/bin:/bin:/usr/local/bin"

#COLOR FORMATTING
YELLOW="\e[93m"
RED='\e[31m'
NC="\e[39m"

CD__SERVICE_ACCOUNT_NAME="tiller-deployer"
CD__NAMESPACE="cd"

APP__SERVICE_ACCOUNT_NAME="tiller-deployer"

SCRIPTS_PATH=./scripts


#####################
# APP CLUSTER SETUP #
#####################
echo -e "${RED}=== APP'S CLUSTER SETUP ===\n${NC}"

# Create all kubernetes components for APP namespace in cluster A
export KUBECONFIG=$CD_KUBECONFIG
bash ${SCRIPTS_PATH}/create_k8s_components.sh ${APP__SERVICE_ACCOUNT_NAME} ${APP__NAMESPACE}

# Setup Gitlab Registry
bash ${SCRIPTS_PATH}/registry_setup.sh ${REGISTRY_TOKEN} ${APP__NAMESPACE}

# Generate kubeconfig (INACTIVE)
## Cannot store kubeconfig in secret till
## gitlab-runner allow to inject custom secret
#bash ${SCRIPTS_PATH}/generate_config.sh ${APP__SERVICE_ACCOUNT_NAME}  ${APP__NAMESPACE}


######################
# CD NAMESPACE SETUP #
######################
echo -e "${RED}=== CONTINUOUS DEPLOYMENT'S CLUSTER SETUP ===\n${NC}"

# Create all kubernetes components for CD namespace in cluster B
export KUBECONFIG=$CI_KUBECONFIG
bash ${SCRIPTS_PATH}/create_k8s_components.sh ${CD__SERVICE_ACCOUNT_NAME} ${CD__NAMESPACE}

# Setup Gitlab Registry
bash ${SCRIPTS_PATH}/registry_setup.sh ${REGISTRY_TOKEN} ${CD__NAMESPACE}

# Install gilab-runner
cp values.yaml cd_custom_values.yaml
sed -i 's/GROUP_TOKEN/'"${CD__GROUP_TOKEN}"'/g' cd_custom_values.yaml
sed -i 's|JOB_TAGS_LIST|continuous-deploy|g' cd_custom_values.yaml
helm repo add gitlab https://charts.gitlab.io
helm --namespace ${CD__NAMESPACE}  del gitlab-runner || echo;
helm install  gitlab-runner \
	 --namespace ${CD__NAMESPACE} \
	 -f cd_custom_values.yaml \
	 gitlab/gitlab-runner
rm -f cd_custom_values.yaml
