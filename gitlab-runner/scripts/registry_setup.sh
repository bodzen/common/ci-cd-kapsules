#!/bin/bash

#set -e
export PATH="/usr/bin:/bin:/usr/local/bin"

#COLOR FORMATTING
BLUE="\e[34m"
RED='\e[31m'
NC="\e[39m"


if [[ -z "$1" ]] || [[ -z "$2" ]]
then
	echo -e "${RED}usage: $0 <REGISTRY TOKEN> <NAMESPACE>${NC}"
	exit
fi

REGISTRY_TOKEN="$1"
NAMESPACE="$2"
REGISTRY_URL="registry.gitlab.com"
REGISTRY_USERNAME="dh4rm4"
REGISTRY_EMAIL="dh4rm4@protonmail.com"


# Setup Gitlab Registry
echo -e "${BLUE}Setting up gitlab-registry...${NC}"
kubectl get secrets gitlab-registry 1>&-
if [ $? -eq 1 ];
then
	kubectl -n ${NAMESPACE} \
			create secret docker-registry gitlab-registry \
			--docker-server=${REGISTRY_URL} \
			--docker-username=${REGISTRY_URL} \
			--docker-password=${REGISTRY_TOKEN} \
			--docker-email=${REGISTRY_EMAIL} || echo
else
	echo -e "${YELLOW}Regsitry alreay setup${NC}"
fi
