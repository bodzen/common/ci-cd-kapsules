#!/bin/bash
set -e
set -o pipefail

export PATH="/usr/bin:/bin:/usr/local/bin"

if [[ -z "$1" ]] || [[ -z "$2" ]]; then
	echo "usage: $0 <service_account_name> <namespace>"
	exit 1
fi

# Install dependancies
if ! [ -x "$(command -v jq)" ];
then
	echo "Need root to install jq"
	sudo apt install jq
fi


#COLOR FORMATTING
BLUE="\e[34m"
GREEN="\e[32m"
YELLOW="\e[93m"
NC="\e[39m"
#
SERVICE_ACCOUNT_NAME=$1
NAMESPACE="$2"
KUBECFG_FILE_NAME="k8s-${SERVICE_ACCOUNT_NAME}-${NAMESPACE}-kubeconf"

get_secret_name_from_service_account() {
    echo -e "${BLUE}Getting secret of service account ${SERVICE_ACCOUNT_NAME} on ${NAMESPACE}"
    SECRET_NAME=$(kubectl get sa "${SERVICE_ACCOUNT_NAME}" --namespace="${NAMESPACE}" -o json | jq -r .secrets[].name)
    echo "Secret name: ${SECRET_NAME}"
}

extract_ca_crt_from_secret() {
    echo -e -n "${BLUE}Extracting ca.crt from secret..."
    kubectl get secret --namespace "${NAMESPACE}" "${SECRET_NAME}" -o json | jq \
    -r '.data["ca.crt"]' | base64 -d > "ca.crt"
    echo -e "${GREEN}\tdone${NC}"
}

get_user_token_from_secret() {
    echo -e -n "${BLUE}Getting user token from secret...${NC}"
    USER_TOKEN=$(kubectl get secret --namespace "${NAMESPACE}" "${SECRET_NAME}" -o json | jq -r '.data["token"]' | base64 -d)
    echo -e "${GREEN}\tdone${NC}"
}

set_kube_config_values() {
    context=$(kubectl config current-context)
    echo -e "${BLUE}Setting current context to: $context${NC}"

    CLUSTER_NAME=$(kubectl config get-contexts "$context" | awk '{print $3}' | tail -n 1)
    echo "Cluster name: ${CLUSTER_NAME}"

    ENDPOINT=$(kubectl config view \
    -o jsonpath="{.clusters[?(@.name == \"${CLUSTER_NAME}\")].cluster.server}")
    echo "Endpoint: ${ENDPOINT}"

    # Set up the config
    echo -e "${BLUE}Preparing k8s-${SERVICE_ACCOUNT_NAME}-${NAMESPACE}-conf"
    echo -e "Setting a cluster entry in kubeconfig...${NC}"
    kubectl config set-cluster "${CLUSTER_NAME}" \
    --kubeconfig="${KUBECFG_FILE_NAME}" \
    --server="${ENDPOINT}" \
    --certificate-authority="ca.crt" \
    --embed-certs=true

    echo -e "Setting token credentials entry in kubeconfig..."
    kubectl config set-credentials \
    "${SERVICE_ACCOUNT_NAME}-${NAMESPACE}-${CLUSTER_NAME}" \
    --kubeconfig="${KUBECFG_FILE_NAME}" \
    --token="${USER_TOKEN}"

    echo -e "${BLUE}Setting a context entry in kubeconfig...${NC}"
    kubectl config set-context \
    "${SERVICE_ACCOUNT_NAME}-${NAMESPACE}-${CLUSTER_NAME}" \
    --kubeconfig="${KUBECFG_FILE_NAME}" \
    --cluster="${CLUSTER_NAME}" \
    --user="${SERVICE_ACCOUNT_NAME}-${NAMESPACE}-${CLUSTER_NAME}" \
    --namespace="${NAMESPACE}"

    echo -e "${BLUE}Setting the current-context in the kubeconfig file...${NC}"
    kubectl config use-context "${SERVICE_ACCOUNT_NAME}-${NAMESPACE}-${CLUSTER_NAME}" \
			--kubeconfig="${KUBECFG_FILE_NAME}"
	echo -e "${GREEN}Kubeconfig generate, see: ${YELLOW}${KUBECFG_FILE_NAME}${NC}\n"
}

cleaner() {
	rm ca.crt
}

get_secret_name_from_service_account
extract_ca_crt_from_secret
get_user_token_from_secret
set_kube_config_values
cleaner
