#!/bin/bash
set -e
set -o pipefail

export PATH="/usr/bin:/bin:/usr/local/bin"
#COLOR FORMATTING
BLUE="\e[34m"
RED="\033[31m"
NC="\e[39m"


if [[ -z "$1" ]] || [[ -z "$2" ]]; then
	echo -e "${RED}usage: $0 <service_account_name> <namespace>${NC}"
	exit 1
fi

SERVICE_ACCOUNT_NAME=$1
NAMESPACE="$2"
MANIFEST_PATH=./manifests

create_namespace () {
	echo -e "${BLUE}Creating namespace: ${NAMESPACE}${NC}"
	kubectl create namespace ${NAMESPACE} || echo;
}

create_service_account() {
    echo -e "${BLUE}Creating a service account in ${NAMESPACE} namespace: ${SERVICE_ACCOUNT_NAME}${NC}"
    kubectl create sa "${SERVICE_ACCOUNT_NAME}" --namespace "${NAMESPACE}" || echo ;
}

create_serviceaccount_role() {
	echo -e "${BLUE}Creating ${SERVICE_ACCOUNT_NAME}'s role.${NC}"
	cp ${MANIFEST_PATH}/tiller_role_service_account_rbac.yaml ${MANIFEST_PATH}/custom_tiller_role.yaml
	sed -i 's/K8S_NAMESPACE/'"${NAMESPACE}"'/g' ${MANIFEST_PATH}/custom_tiller_role.yaml
	kubectl apply -f ${MANIFEST_PATH}/custom_tiller_role.yaml
	rm -f ${MANIFEST_PATH}/custom_tiller_role.yaml
}

bind_tiller_role_to_serviceaccount() {
	echo -e "${BLUE}Binding tiller's role to ${SERVICE_ACCOUNT_NAME} serviceaccount${NC}"
	cp ${MANIFEST_PATH}/bind_role_and_service_account.yaml ${MANIFEST_PATH}/custom_bind.yaml
	sed -i 's/K8S_NAMESPACE/'"${NAMESPACE}"'/g' ${MANIFEST_PATH}/custom_bind.yaml
	kubectl apply -f ${MANIFEST_PATH}/custom_bind.yaml
	rm -f ${MANIFEST_PATH}/custom_bind.yaml
}


create_namespace
create_service_account
create_serviceaccount_role
bind_tiller_role_to_serviceaccount
