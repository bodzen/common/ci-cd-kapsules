#!/bin/bash

major_output() {
	echo -e "${YELLOW}=== $@ ===\n${NC}"
}

simple_output() {
	echo -e "[${YELLOW}+${NC}] ${BLUE}$@${NC}"
}

error_output() {
	echo -e "[${RED}-${NC}] ${BLUE}$@${NC}"
}
