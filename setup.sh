#!/bin/bash

set -e

####################
# INPUT VALIDATION #
####################

source variables.txt
source output_funcs.sh

major_output "CHECKING INPUTS"

VARIABLES_MUST_BE_DEFINED="TF_VAR_SCALEWAY_ACCESS_KEY TF_VAR_SCALEWAY_SECRET_KEY TF_VAR_SCALEWAY_ORGANISATION_ID CI__GROUP_TOKEN CD__GROUP_TOKEN REGISTRY_TOKEN APP__NAMESPACE"

for variable in ${VARIABLES_MUST_BE_DEFINED}
do
	if [ -z "${!variable}" ]
	then
		error_output "Missing environment variable $variable"
		exit 1
	fi
done

COMMAND_MUST_BE_INSTALLED="kubectl terraform helm xclip jq"
for cmd in ${COMMAND_MUST_BE_INSTALLED}
do
	if ! command -v $cmd &> /dev/null
	then
		error_output "$cmd binary must be installed"
		exit 1
	fi
done

simple_output "Inputs are valid\n"

################
# PROVISIONING #
################

major_output "STARTING CLUSTERS PROVISIONING"

cd ./provisioning
terraform init -input=false
terraform validate
terraform plan -out=tf_plan -input=false
terraform apply -input=false -state=kapsules.tfstate tf_plan

if ! test -f "ci-kapsule_kubeconfig" || ! test -f "cd-kapsule_kubeconfig"
then
	error_output "Something went wrong during provisioning: Kubeconfig files are missing."
fi

cd ..

simple_output "Waiting 60 seconds while clusters' pool are being setup\n\n"
sleep 60

#################
# GITLAB-RUNNER #
#################

major_output "STARTING GITLAB-RUNNER SETUP"

ROOT_DIR=`pwd`

cd ./gitlab-runner
export CI_KUBECONFIG="$ROOT_DIR/provisioning/ci-kapsule_kubeconfig"
export CD_KUBECONFIG="$ROOT_DIR/provisioning/cd-kapsule_kubeconfig"
simple_output "Setting up CI cluster"
bash setup_ci.sh
simple_output "Setting up CD cluster"
bash setup_cd.sh


simple_output "Generating CD kubeconfig"
export KUBECONFIG="$ROOT_DIR/provisioning/cd-kapsule_kubeconfig"
bash scripts/generate_config.sh tiller-deployer ${APP__NAMESPACE}
base64 k8s-tiller-deployer-${APP__NAMESPACE}-kubeconf | tr -d '\n' | xclip -selection c
rm -f k8s-tiller-deployer-${APP__NAMESPACE}-kubeconf $CI_KUBECONFIG $CD_KUBECONFIG

major_output "Everything seems to work"

simple_output "The kubeconfig have been generated and stored in your clipboard."
simple_output "Proceed to store it in your cd gitlab group variable${NC}"
