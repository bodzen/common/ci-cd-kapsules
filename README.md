# CI-CD Kapsules

Provision and setup two k8s cluster to integrate Gitlab's CI/CD.

One cluster will be use to do all the CI/CD jobs, while the other one will be the destination of the CD pipelines.

## How To

You must define all the variables founed in the [variables.txt]() :
* [TF_VAR_SCALEWAY_ACCESS_KEY](https://console.scaleway.com/account/organization/credentials)
* [TF_VAR_SCALEWAY_SECRET_KEY](https://console.scaleway.com/account/organization/credentials)
* [TF_VAR_SCALEWAY_ORGANISATION_ID](https://console.scaleway.com/account/organization/credentials)
* CI__GROUP_TOKEN: runner registration token for your CI group
* CD__GROUP_TOKEN: runner registration token for your CD group
* [REGISTRY_TOKEN](https://gitlab.com/profile/personal_access_tokens): gitlab registry token with read only right
* APP__NAMESPACE: namespace used to deploy all apps in CD pipelines
