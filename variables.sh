#!/bin/bash

export TF_VAR_SCALEWAY_ACCESS_KEY=
export TF_VAR_SCALEWAY_SECRET_KEY=
export TF_VAR_SCALEWAY_ORGANISATION_ID=
export CI__GROUP_TOKEN=
export CD__GROUP_TOKEN=
export REGISTRY_TOKEN=
export APP__NAMESPACE=

export YELLOW="\e[93m"
export RED='\e[31m'
export NC="\e[39m"
export BLUE="\e[34m"
