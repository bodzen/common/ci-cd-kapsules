resource "scaleway_k8s_cluster_beta" "kapsule" {
	version = var.KAPSULE_CONF.version
	name = var.KAPSULE_CONF.name

	cni = var.KAPSULE_CONF.cni

	enable_dashboard = var.KAPSULE_CONF.enable_dashboard

	tags = var.KAPSULE_CONF.tags
}

resource "scaleway_k8s_pool_beta" "kapsule" {
	cluster_id = scaleway_k8s_cluster_beta.kapsule.id
	name = var.POOL_CONF.name

	node_type = var.POOL_CONF.node_type
	size = var.POOL_CONF.size

	autoscaling = var.POOL_CONF.autoscaling
	autohealing = var.POOL_CONF.autohealing

	min_size = var.POOL_CONF.autoscaling ? var.POOL_CONF.min_size : var.POOL_CONF.size
	max_size = var.POOL_CONF.autoscaling ? var.POOL_CONF.max_size : var.POOL_CONF.size
}
