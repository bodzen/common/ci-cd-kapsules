##########
# MAIN.TF

variable "DATACENTER_INFOS" {
	description = "Datacenter location"

	type = object({
		zone   = string
		region = string
	})

	default = {
		zone   = "fr-par-1"
		region = "fr-par"
	}
}

variable "SCALEWAY_ACCESS_KEY" {
	type = string
}

variable "SCALEWAY_SECRET_KEY" {
	type = string
}

variable "SCALEWAY_ORGANISATION_ID" {
	description = "Scaleway account's organisation"
	type        = string
}

#############
# KAPSULES

variable "KAPSULE_CONF" {
	description = "Cluster configuration for kapsule resource"
	type = object({
		version = string
		name = string
		cni = string
		enable_dashboard = bool
		tags = list(string)
	})

	default = {
		version = "1.18.8"
		name = "my-kapsule"
		cni  = "cilium"
		enable_dashboard = false
		tags = ["my-kapsule"]
	}
}

variable "POOL_CONF" {
	description = "Pool configuration for kapsule cluster"
	type = object({
		name = string

		node_type = string
		size = number

		autoscaling = bool
		autohealing = bool
		min_size = number
		max_size = number

	})

	default = {
		name = "my-kapsule-pool"

		node_type = "DEV1-M"
		size = 1

		autoscaling = false
		autohealing = true
		min_size = 1
		max_size = 5
	}
}
