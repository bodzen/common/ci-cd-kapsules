resource "local_file" "kubeconfig" {
  content = scaleway_k8s_cluster_beta.kapsule.kubeconfig[0].config_file
  filename = "${var.KAPSULE_CONF.name}_kubeconfig"
}
