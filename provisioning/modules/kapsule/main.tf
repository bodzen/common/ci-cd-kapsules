provider "scaleway" {
	access_key      = var.SCALEWAY_ACCESS_KEY
	secret_key      = var.SCALEWAY_SECRET_KEY
	organization_id = var.SCALEWAY_ORGANISATION_ID

	zone            = var.DATACENTER_INFOS.zone
	region          = var.DATACENTER_INFOS.region

	version         = "~> 1.16.0"
}
