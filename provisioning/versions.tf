terraform {
  required_providers {
    scaleway = {
      source = "terraform-providers/scaleway"
    }
  }
  required_version = ">= 0.13"
}
