variable "SCALEWAY_ACCESS_KEY" {}
variable "SCALEWAY_SECRET_KEY" {}
variable "SCALEWAY_ORGANISATION_ID" {}


module "ci-kapsule" {
	source = "./modules/kapsule"

	KAPSULE_CONF = {
		version = "1.18.8"
		name = "ci-kapsule"
		cni  = "cilium"
		enable_dashboard = false
		tags = ["ci-kapsule", "gitlab-runners"]
	}
	POOL_CONF = {
		name = "ci-kapsule-pool"
		node_type = "DEV1-M"
		size = 1
		autoscaling = false
		autohealing = true
		min_size = 1
		max_size = 5
	}
	SCALEWAY_ACCESS_KEY = "${var.SCALEWAY_ACCESS_KEY}"
	SCALEWAY_SECRET_KEY = "${var.SCALEWAY_SECRET_KEY}"
	SCALEWAY_ORGANISATION_ID = "${var.SCALEWAY_ORGANISATION_ID}"
}

module "cd-kapsule" {
	source = "./modules/kapsule"

	KAPSULE_CONF = {
		version = "1.18.8"
		name = "cd-kapsule"
		cni  = "cilium"
		enable_dashboard = false
		tags = ["cd-kapsule", "gitlab-runners"]
	}
	POOL_CONF = {
		name = "cd-kapsule-pool"
		node_type = "DEV1-M"
		size = 1
		autoscaling = false
		autohealing = true
		min_size = 1
		max_size = 5
	}
	SCALEWAY_ACCESS_KEY = "${var.SCALEWAY_ACCESS_KEY}"
	SCALEWAY_SECRET_KEY = "${var.SCALEWAY_SECRET_KEY}"
	SCALEWAY_ORGANISATION_ID = "${var.SCALEWAY_ORGANISATION_ID}"
}
